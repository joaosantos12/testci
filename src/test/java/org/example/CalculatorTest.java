package org.example;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

class CalculatorTest {

    @Test
    void add() {
        assertEquals(2, Calculator.add(1, 1));
    }

    @Test
    void substract() {
        assertEquals(0, Calculator.substract(1, 1));
    }

    @Test
    void multiply() {
        assertEquals(1, Calculator.multiply(1, 1));
    }

    @Test
    void divide() {
        assertEquals(1, Calculator.divide(1, 1));
    }
}